![Recipppe](https://trello-attachments.s3.amazonaws.com/50ff46f67c31cea31d0011ec/53551b8d837b1a4f28540bf7/141x80/f40ce7c1c93be0b1f9ecdf94c8577040/Recipppe.gif)

Our idea is to build a platform where you can create and share your recipes in a very intuitive application.

###### Application uses:

* Rails 4.0.2
* MongoDB
* Thin

### To run MongoDB daemonized:

    mongod --fork --config ./db/mongodb-dev.conf
