require 'spec_helper'

describe UsersController do
  
  before do
    request.env['omniauth.auth'] = env_auth
  end
  
  describe 'GET #create' do
    it do
      expect {
        get :create
      }.to change(User, :count).by(1)
    end
    it 'should create user session cookie' do
      get :create
      expect(response.cookies['token']).to eq(User.last.id.to_s)
    end
    describe 'when invalid auth' do
      before {
        request.env['omniauth.auth'] = {}
        get :create
      }
      it { should set_the_flash[:notice].to('Something went wrong, please try again.') }
    end
  end
  
end