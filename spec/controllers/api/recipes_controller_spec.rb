require 'spec_helper'

describe Api::RecipesController do

  before do
    @user = User.from_omniauth(env_auth)
    @category = FactoryGirl.create(:category)
    authenticate(@user)
  end

  let(:recipe) { FactoryGirl.create(:recipe, user: @user, category: @category) }
  let(:cover_image) do
    Rack::Test::UploadedFile.new(File.open(File.join(Rails.root, '/spec/fixtures/covers/cover.jpg')))
  end

  describe 'PATCH #update in json format' do
    before do
      @recipe = FactoryGirl.create(:recipe, user: @user, category: @category, servings: 1, preparation_time: 30)
    end
    it 'should update recipe line_items' do
      patch :update, format: :json, id: @recipe.id, recipe: { name: 'Updated Recipe',
        line_items_attributes: [
          {
            item_type: 'list',
            tag: 'ol',
            name: 'a1f3',
            content: 'Just a test',
            position: 0
          },
          {
            item_type: 'single',
            tag: 'h2',
            name: 'b7g9',
            content: 'Another test',
            position: 1
          }
        ]
      }
      expect(@recipe.reload.name).to eq 'Updated Recipe'
      expect(@recipe.reload.line_items.size).to eq 2
    end
  end

  # describe '#cover' do
  #   it 'should upload cover photo' do
  #     expect {
  #       post :cover, format: :json, recipe_id: recipe.id, cover_image: [cover_image]
  #     }.to change {recipe.reload.cover_image.url}
  #   end
  # end

end
