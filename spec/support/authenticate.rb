def authenticate(user)
  controller.stub(:current_user).and_return(user)
end