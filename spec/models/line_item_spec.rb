require 'spec_helper'

describe LineItem do

  describe :validations do
    it { should validate_presence_of(:item_type) }
    it { should ensure_inclusion_of(:item_type).in_array(LineItem::ITEM_TYPES) }
    it { should validate_presence_of(:tag) }
  end

  let(:user) { User.from_omniauth(env_auth) }
  let(:category) { FactoryGirl.create(:category) }
  let(:recipe) { FactoryGirl.create(:recipe, user: user, category: category) }
  let(:line_item) do
    recipe.line_items.create({
      item_type: 'list',
      tag: 'ol',
      name: 'a1b1',
      content: '<li>Just a test...</li>'
    })
  end

  describe :validations do
    it { should validate_presence_of(:item_type) }
    it { should validate_presence_of(:name) }
  end

  it 'should create a line item' do
    expect(line_item.persisted?).to be_true
    expect(line_item.id).to eq 'a1b1'
  end

end
