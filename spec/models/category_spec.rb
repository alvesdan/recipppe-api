require 'spec_helper'

describe Category do
  
  describe :associations do
    it 'should have many recipes' do
      expect(Category.new.respond_to?(:recipes)).to be_true
    end
  end
  
  describe :validations do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:slug) }
  end
  
  describe 'CREATE' do
    it 'should generate slug' do
      category = Category.create(name: 'First Category')
      expect(category.slug).to eq('first-category')
    end
  end
  
end
