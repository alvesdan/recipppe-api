require 'spec_helper'

describe Recipe do

  let(:user) { User.from_omniauth(env_auth) }
  let(:category) { FactoryGirl.create(:category) }
  let(:sample_recipe) do
    {
      name: 'Sample Recipe',
      category_id: category.id,
      user_id: user.id,
      servings: '2',
      preparation_time: '30',
      published: true,
      line_items_attributes: [
        {
          name: 'a1b1',
          item_type: 'list',
          tag: 'ol',
          content: 'My first list item'
        },
        {
          name: 'a1b2',
          item_type: 'list',
          tag: 'ul',
          content: 'My second list item'
        }
      ]
    }
  end
  let(:recipe) { Recipe.create(sample_recipe) }

  describe :associations do
    it 'should have many steps' do
      expect(Recipe.new.respond_to?(:line_items)).to be_true
    end
  end

  describe :validations do
    it { should validate_presence_of(:user) }
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:category).on(:update) }
    it { should validate_presence_of(:shortened_url).on(:update) }
    it { should validate_presence_of(:servings).on(:update) }
    it { should validate_presence_of(:preparation_time).on(:update) }
  end

  describe 'CREATE' do
    it 'should auto increment sequence' do
      expect(recipe.sequence).to be > 99
    end
  end

  describe 'Create recipe with nested line items' do
    it 'should be able to create all line items' do
      expect(recipe.line_items.count).to eq 2
    end
  end

  describe 'UPDATE' do
    it 'should update steps' do
      recipe.update({
        line_items_attributes: [
          {
            name: 'a1b1',
            item_type: 'list',
            tag: 'ol',
            content: 'Line item with updated content'
          }
        ]
      })
      expect(recipe.line_items.size).to eq 2
      expect(recipe.line_items.first.content).to match(/updated content/)
    end
  end

  describe 'DESTROY line item' do
    it 'should destroy line item by name' do
      recipe.update({
        line_items_attributes: [{ name: 'a1b1', _destroy: '1' }]
      })
      expect(recipe.line_items.size).to eq 1
    end
  end

end
