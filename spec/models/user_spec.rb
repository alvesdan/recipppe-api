require 'spec_helper'

describe User do
  
  describe :validations do
    it { should validate_presence_of(:uid) }
    it { should validate_presence_of(:username) }
    it { should validate_presence_of(:token) }
    it { should validate_presence_of(:email) }
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:profile_image) }
    it { should validate_uniqueness_of(:uid) }
    it { should validate_uniqueness_of(:username) }
    it { should validate_uniqueness_of(:token) }
    it { should validate_uniqueness_of(:email) }
  end
  
  describe 'CREATE' do
    before do
      @user = User.from_omniauth(env_auth)
    end
    it 'should create from omniauth' do
      expect(@user.persisted?).to be_true
    end
    it 'should create with correct credentials' do
      expect(@user.name).to eq 'Joe Bloggs'
      expect(@user.uid).to eq '1234567ABCDEF'
    end
  end
  
end
