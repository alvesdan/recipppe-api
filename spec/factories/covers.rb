FactoryGirl.define do
  factory :cover do
    cover_image Rack::Test::UploadedFile.new(File.open(File.join(Rails.root, '/spec/fixtures/covers/cover.jpg')))
  end
end
