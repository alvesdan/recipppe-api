class CoverImageUploader < CarrierWave::Uploader::Base

  include CarrierWave::RMagick
  storage Rails.env.test? ? :file : :fog

  def store_dir
    "uploads/recipes/covers/images/#{model.id}"
  end

  version :thumb do
    resize_to_fit(400, 300)
    process convert: 'jpg', quality: 50
  end

  version :medium do
    resize_to_fill(800, 600)
    process convert: 'jpg', quality: 60
  end

  version :large do
    resize_to_fill(1280, 960)
    process convert: 'jpg', quality: 80
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end

  def filename
    @name ||= "#{Digest::SHA1.hexdigest(original_filename)}.jpg" if original_filename
  end

end
