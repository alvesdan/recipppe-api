class RecipeImageUploader < CarrierWave::Uploader::Base

  include CarrierWave::RMagick
  storage Rails.env.test? ? :file : :fog

  def store_dir
    "uploads/recipes/images/#{model.id}"
  end

  version :large do
    resize_to_fit(600, 10000)
    process convert: 'jpg', quality: 80
  end

  version :medium do
    resize_to_fill(400, 300)
    process convert: 'jpg', quality: 70
  end

  version :thumb do
    resize_to_fill(150, 150)
    process convert: 'jpg', quality: 50
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end

  def filename
    @name ||= "#{Digest::SHA1.hexdigest(original_filename)}.jpg" if original_filename
  end

end
