class LineItem
  include Mongoid::Document

  ITEM_TYPES = ['list', 'single', 'image']

  field :item_type, type: String, default: 'list'
  field :tag, type: String
  field :content, type: String
  field :position, type: Integer, default: 100
  field :name, type: String
  field :_id, type: String, default: -> { name }

  validates :item_type, :name, :tag, presence: true
  validates :item_type, inclusion: { in: ITEM_TYPES }
  default_scope -> { order_by([:position]) }
  embedded_in :recipe

end
