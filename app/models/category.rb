class Category
  
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :name, type: String
  field :slug, type: String
  
  has_many :recipes
  before_validation :generate_slug
  validates :name, :slug, presence: true
  
protected
  
  def generate_slug
    self.slug ||= name.parameterize if name.present?
  end
  
end
