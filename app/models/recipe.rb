class Recipe

  require 'base62'
  include Mongoid::Document
  include Mongoid::Timestamps

  auto_increment :sequence, seed: 100
  field :user_id, type: String
  field :category_id, type: String
  field :name, type: String
  field :shortened_url, type: String
  field :servings, type: Integer, default: 2
  field :preparation_time, type: Integer, default: 30
  field :published, type: Boolean, default: false

  mount_uploader :cover_image, CoverImageUploader

  belongs_to :user
  belongs_to :category
  embeds_many :line_items
  embeds_many :recipe_images
  accepts_nested_attributes_for :line_items, allow_destroy: true

  validates :user, :name, presence: true
  validates :category, :shortened_url, :servings, :preparation_time, presence: true, on: :update

  before_validation :set_default_category
  after_create :generate_shortened_url, :create_default_line_item

  def cover_image_large_url
    cover_image.url(:large)
  end

  def update(attrs)
    line_items_attributes = attrs.delete(:line_items_attributes)
    super(attrs)
    update_line_items(line_items_attributes) if line_items_attributes
  end

  # FIXME: Spec will not pass since we do not expect paramenters
  # contain an array instead of a simple hash
  # That's why we are using a map(:&last) here
  def update_line_items(line_items_attributes)
    line_items_attributes.map(&:last).each do |attrs|
      if attrs.has_key?(:_destroy)
        destroy_line_item(attrs[:name])
      else
        find_or_create_line_item(attrs)
      end
    end
  end

protected

  def set_default_category
    self.category_id ||= Category.first.id
  end

  def create_default_line_item
    line_items.create({
      item_type: 'single',
      tag: 'h2',
      content: '',
      name: SecureRandom.hex(2)
    })
  end

  def generate_shortened_url
    update_attribute(:shortened_url, self.sequence.base62_encode)
  end

  def find_or_create_line_item(attrs)
    line_item = line_items.where(name: attrs[:name]).first_or_initialize
    line_item.assign_attributes(attrs) and line_item.save
    line_item
  end

  def destroy_line_item(name)
    line_items.where(name: name).try(:first).try(:destroy)
  end

end
