class User

  include Mongoid::Document
  include Mongoid::Timestamps

  field :uid, type: String
  field :username, type: String
  field :token, type: String
  field :email, type: String
  field :name, type: String
  field :profile_image, type: String
  field :approved, type: Boolean, default: false
  field :location, type: String
  field :optin, type: Boolean, default: true
  field :confirmation_token, type: String
  field :confirmed_at, type: DateTime
  field :utm_source, type: String
  field :utm_campaign, type: String
  field :utm_medium, type: String
  field :utm_content, type: String
  field :utm_term, type: String

  validates :uid, :username, :token, :email, :name, :profile_image, presence: true
  validates :uid, :username, :token, :email, uniqueness: true

  has_many :recipes

  def self.from_omniauth(auth)
    find_or_create_by!(uid: auth['uid']) do |u|
      u.username = auth['extra']['raw_info']['username']
      u.token = auth['credentials']['token']
      u.email = auth['info']['email']
      u.name = auth['info']['name']
      u.profile_image = auth['info']['image']
      u.location = auth['info']['location']
    end rescue User.new
  end

  def large_profile_image
    "#{profile_image}?type=large"
  end

end
