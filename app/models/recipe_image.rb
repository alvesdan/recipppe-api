class RecipeImage

  include Mongoid::Document
  include Mongoid::Timestamps

  embedded_in :recipe
  mount_uploader :image, RecipeImageUploader
end
