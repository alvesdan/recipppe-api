class RecipeImagesUploadService

  attr_reader :recipe, :image
  def initialize(recipe, image)
    @recipe = recipe
    @image = image
  end

  def upload_image
    recipe.recipe_images.create(image: image)
  end
end
