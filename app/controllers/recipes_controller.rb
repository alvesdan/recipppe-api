class RecipesController < ApplicationController

  before_action :authenticate_user!
  before_action :load_recipe, only: [:edit, :show, :destroy]

  def index
    @recipes = current_user.recipes.order(created_at: :desc)
  end

  def create
    @recipe = current_user.recipes.find_or_create_by(recipe_params)

    redirect_to after_create_path
  end

  def edit; end
  def show; end

  def destroy
    @recipe.destroy
    redirect_to recipes_path
  end

  protected

  def recipe_params
    params.require(:recipe).permit(:name)
  end

  def load_recipe
    @recipe = current_user.recipes.find(params[:id])
  end

  def after_create_path
    @recipe.persisted? ? edit_recipe_path(@recipe) : recipes_path
  end

end
