class UsersController < ApplicationController

  def new
    redirect_to recipes_path if user_signed_in?
  end

  def create
    @user = User.from_omniauth(request.env['omniauth.auth'].deep_stringify_keys!)
    if @user.persisted?
      sign_in(@user)
    else
      Rails.logger.debug(@user)
      flash[:notice] = 'Something went wrong, please try again.'
    end
    redirect_to root_path
  end

end
