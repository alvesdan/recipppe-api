class ApplicationController < ActionController::Base

  protect_from_forgery with: :exception

  def authenticate_user!
    redirect_to root_path unless current_user
  end

  def sign_in(user)
    cookies[:token] = user.id
  end

  def sign_out
    cookies.delete :token
  end

  def current_user
    @current_user ||= User.find(cookies[:token]) if cookies[:token]
  rescue
    nil
  end
  helper_method :current_user

  def user_signed_in?
    current_user.present?
  end
  helper_method :user_signed_in?

end
