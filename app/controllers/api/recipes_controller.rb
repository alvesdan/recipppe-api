class Api::RecipesController < ApplicationController

  before_action :authenticate_user!, :load_recipe
  respond_to :json

  def show
    respond_with @recipe.to_json(include: [:line_items], methods: [:cover_image_large_url])
  end

  def update
    @recipe.update(recipe_params)
    respond_with @recipe
  end

  def cover
    @recipe.update({ cover_image: params[:cover_image][0] })
    respond_to do |format|
      format.json {
        @recipe.reload
        render json: @recipe.to_json(methods: [:cover_image_large_url])
      }
    end
  end

protected

  def load_recipe
    @recipe = current_user.recipes.find(params[:id])
  end

  def recipe_params
    params.require(:recipe).permit(:published, :name, :servings, :preparation_time, :category_id, line_items_attributes: [:item_type, :name, :tag, :content, :position, :_destroy])
  end

end
