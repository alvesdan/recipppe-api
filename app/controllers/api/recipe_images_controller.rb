class Api::RecipeImagesController < ApplicationController

  def index
  end

  def create
    respond_to do |format|
      format.json do
        render json: upload_service.upload_image
      end
    end
  end

  def destroy
  end

  private

  def recipe
    @recipe ||= Recipe.find(params[:recipe_id])
  end

  def image_param
    params[:image].first
  end

  def upload_service
    RecipeImagesUploadService.new(recipe, image_param)
  end
end
