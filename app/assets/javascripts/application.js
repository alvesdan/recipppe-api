//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require turbolinks
//= require nprogress
//= require nprogress-turbolinks
//= require bootstrap-sprockets

//= require mustache
//= require bootstrap
//= require dropzone
//= require sortable
//= require layout
//= require editing/recipe
//= require editing/select_field
//= require editing/shared_behaviors
//= require editing/text_field
//= require editing/global_methods
//= require editing/extend_string
//= require editing/line_items/line_item_list
//= require editing/line_items/line_item_single
//= require editing/line_items/line_item_image
//= require editing/select_images
//= require editing/sort
//= require editing/cover
//= require editing/recipe_images
//= require editing/editing
//= require sidebar
//= require recipes
//= require profile


