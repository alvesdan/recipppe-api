'use strict';
var SelectImages;
SelectImages = (function(){

  function SelectImages(receiver, imgIndex) {
    this.receiver = receiver;
    this.selected = [];
    this.imgIndex = imgIndex;
    this.$modalElement = $('#recipeImagesModal');
    this.$modalElement.modal({show: false});
  }

  SelectImages.prototype.initialize = function() {
    this.bindImageClicks();
    this.bindModalClosing();
  }

  SelectImages.prototype.bindImageClicks = function() {
    var _this = this;

    $(_this).on('imageSelected', function(event, imageUrl) {
      _this.selected.push(imageUrl);
      _this.verifySelected();
    });

    $('#recipeImagesModal .thumbnail').on('click', function() {
      var url = $(this).data('url');
      $(_this).trigger('imageSelected', url);
    });

    _this.$modalElement.modal('show');
  }


  SelectImages.prototype.verifySelected = function() {
    var _this = this;
    if (this.selected.length > 0) {
      $(this.receiver).trigger('imageSelected', {
        'selectedImage': this.selected[0],
        'imgIndex': this.imgIndex
      });
      _this.$modalElement.modal('hide');
    }
  }

  SelectImages.prototype.bindModalClosing = function() {
    var _this = this;
    _this.$modalElement.addClass('selecting');
    _this.$modalElement.on('hidden.bs.modal', function(event) {
      _this.$modalElement.removeClass('selecting');
      $('#recipeImagesModal .thumbnail').off('click');
      $(_this).off('imageSelected');
    });
  }

  return SelectImages;
})();
