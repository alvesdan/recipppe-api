'use strict';
var Editing;
Editing = function() {

  var editing = {};

  editing.init = function() {
    window.recipe = new Recipe();
    window.recipe.synchronizePull();
    window.sort = new Sort($defaultTarget[0]);
    this.createSelectFields();
    this.createTextFields();
    this.createTools();
    this.createCoverSelection();
    this.createRecipeImagesUpload();
  }

  editing.createSelectFields = function() {
    $('.recipe-select-field').each(function(){
      var selectField = new SelectField(this);
    });
    $('[data-toggle=dropdown]').dropdown();
  }

  editing.createTextFields = function() {
    $('.recipe-text-field').each(function() {
      var textField = new TextField(this);
    });
  }

  editing.createTools = function() {
    $('.open-tools').on('click', function() {
      $('#toolsModal').modal({show: true});
    });

    $('#toolsModal li.line-item-tool a').click(function(event) {
      event.preventDefault();
      var type = $(this).data('type');
      var tag = $(this).data('tag');
      $('#toolsModal').modal('hide');
      recipe.addLineItem(type, tag);
    });

    $('.sort-tool a').click(function(event) {
      event.preventDefault();
      $(this).toggleClass('active');
      window.sort.toggle();
    });
  }

  editing.createCoverSelection = function() {
    $('.cover-upload .clickable').on('click', function(){
      $('#recipeImagesModal').modal({show: true});
    });
  }

  editing.createRecipeImagesUpload = function() {
    var recipeImagesUpload = new RecipeImages();
  }

  return editing;

};
