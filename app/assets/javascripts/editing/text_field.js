'use strict';
var TextField;
TextField = (function() {

  var sharedBehaviors = null;

  function TextField(element) {
    this.$element = $(element);
    this.initialize();
  }

  TextField.prototype.initialize = function() {
    sharedBehaviors = new SharedBehaviors(this.$element);
    sharedBehaviors.preventReturnClick();
    sharedBehaviors.cleanPasteFromClipboard();
    this.fieldName = this.$element.attr('data-name');
    this.onKeyUp();
  }

  TextField.prototype.onKeyUp = function() {
    var klass = this;
    this.$element.on('keyup', function() {
      var value = $(this).html().trim().cleanHTML();
      recipe.updateAtribute(klass.fieldName, value);
    });
  }

  return TextField;

})();
