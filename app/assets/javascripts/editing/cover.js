'use strict';
var Cover;
Cover = (function(){

  function Cover() {
    this.headerBackground = $('.header-background');
    this.loadUploadedCover();
    this.coverDropZone = this.createDropZone();
    this.bindProcessing();
    this.bindSuccess();
    this.bindComplete();
  }

  Cover.prototype.clearUploadedCover = function() {
    this.headerBackground.attr('data-background-url', '');
    this.headerBackground.css({'background-image': ''});
  }

  Cover.prototype.loadUploadedCover = function() {
    this.hideCover();
    var backgroundURL = this.headerBackground.data('background-url');
    if (backgroundURL) {
      this.headerBackground.css({
        'background-image': 'url(' + backgroundURL +')'
      });
      this.showCover();
    }
  }

  Cover.prototype.createDropZone = function() {
    return new Dropzone('div.cover-upload', {
      url: '/api/recipes/' + recipeId +'/cover.json',
      method: 'post',
      paramName: 'cover_image',
      maxFiles: 1,
      maxFilesize: 1,
      parallelUploads: 1,
      uploadMultiple: true,
      clickable: '.cover-upload .clickable',
      headers: {
        'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content')
      },
      acceptedFiles: 'image/png,image/jpeg,image/gif',
      previewsContainer: '.cover-upload .upload-details',
      createImageThumbnails: true,
      maxThumbnailFilesize: 1,
      thumbnailWidth: 1280,
      thumbnailHeight: 960
    });
  }

  Cover.prototype.hideCover = function() {
    this.headerBackground.removeClass('display');
  }

  Cover.prototype.showCover = function() {
    this.headerBackground.addClass('display');
  }

  Cover.prototype.bindProcessing = function() {
    var _this = this;
    this.coverDropZone.on('processing', function(_, response) {
      _this.hideCover();
      //$('.cover-upload > span').html('Sending...');
    });
  }

  Cover.prototype.bindSuccess = function() {
    var _this = this;
    this.coverDropZone.on('success', function(_, response) {
      _this.headerBackground.css({
        'background-image': 'url(' + response.cover_image_large_url +')'
      });
      _this.showCover();
    });
  }

  Cover.prototype.bindComplete = function() {
    this.coverDropZone.on('complete', function() {
      //$('.cover-upload > span').html('Drag or <span class="clickable">select</span> a cover photo');
    });
  }

  return Cover;

})();
