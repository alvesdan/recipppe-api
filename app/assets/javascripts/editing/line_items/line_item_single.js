'use strict';
var LineItemSingle;
LineItemSingle = (function() {

  var sharedBehaviors = null;
  var template = '<{{tag}} data-name="{{name}}" data-type="paragraph" data-position="{{position}}" contenteditable="true" class="line-item">{{value}}</{{tag}}>';

  function LineItemSingle(lineItemName, tag) {
    this.itemType = 'single';
    this.lineItemName = lineItemName;
    this.tag = tag;
    this.position = recipe.nextAvailablePosition();
    this.$element = null;
    this.destroy = false;
    this.value = null;
  }

  LineItemSingle.prototype.setValue = function(value) {
    this.value = value;
    this.$element.html(value);
  }

  LineItemSingle.prototype.setPosition = function(position) {
    this.position = position;
  }

  LineItemSingle.prototype.createElementTag = function() {
    this.$element = $(Mustache.render(template, {
      name: this.lineItemName,
      tag: this.tag,
      value: this.value,
      position: this.position
    }));
  }

  LineItemSingle.prototype.setFocus = function() {
    this.$element.focus();
    globalMethods.placeCaretAtEnd(this.$element[0]);
  }

  LineItemSingle.prototype.initialize = function() {
    sharedBehaviors = new SharedBehaviors(this.$element);
    sharedBehaviors.preventReturnClick();
    sharedBehaviors.cleanPasteFromClipboard();
    this.onKeyDown();
    this.onKeyUp();
  }

  LineItemSingle.prototype.onKeyDown = function() {
    var klass = this;
    this.$element.on('keydown', function(event) {
      if (event.keyCode == 13) {
        recipe.addLineItem('single', klass.tag);
      } else if (event.keyCode == 8) {
        if ($(this).html().trim() == '') {
          event.preventDefault();
          klass.destroy = true;
          recipe.removeLineItem(klass.lineItemName);
        }
      }
    });
  }

  LineItemSingle.prototype.onKeyUp = function() {
    var klass = this;
    this.$element.on('keyup', function() {
      klass.value = $(this).html().trim().cleanHTML();
      recipe.synchronizeSave();
    });
  }

  return LineItemSingle;

})();
