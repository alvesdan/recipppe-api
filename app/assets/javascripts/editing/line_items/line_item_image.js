'use strict';
var LineItemImage;
LineItemImage = (function() {

  var baseTemplate = '<div class="line-item image {{tag}} clearfix" data-name={{name}} data-type="image" data-position="{{position}}"><span class="delete">+</span>';
  var templates = {
    'one': '<div class="holder" data-img-index="0"></div>',
    'two': '<div class="holder" data-img-index="0"></div><div class="holder" data-img-index="1"></div>'
  }

  function LineItemImage(lineItemName, tag, value) {
    this.itemType = 'image';
    this.lineItemName = lineItemName;
    this.tag = tag;
    this.position = recipe.nextAvailablePosition();
    this.$element = null;
    this.destroy = false;
    this.value = value || '';
    this.images = {};
  }

  LineItemImage.prototype.setValue = function(value) {
    this.value = value;
    if (value.length) {
      var jsonValue = JSON.parse(value);
      for (var key in jsonValue) {
        var childValue = jsonValue[key];
        this.images[key] = childValue;
        this.$element.find('[data-img-index=' + key + ']').css('background-image', 'url(' + childValue +')');
      }
    }
  }

  LineItemImage.prototype.setPosition = function(position) {
    this.position = position;
  }

  LineItemImage.prototype.createElementTag = function() {
    var mergedTemplate = baseTemplate + templates[this.tag] + '</div>';
    this.$element = $(Mustache.render(mergedTemplate, {
      name: this.lineItemName,
      position: this.position,
      tag: this.tag
    }));
  }

  LineItemImage.prototype.setFocus = function() {
    return true;
  }

  LineItemImage.prototype.initialize = function() {
    this.bindClick();
    this.bindImageSelected();
    this.bindDelete();
  }

  LineItemImage.prototype.bindImageSelected = function() {
    var _this = this;
    $(_this).on('imageSelected', function(event, params) {
      var selectedImage = params['selectedImage'],
          imgIndex = params['imgIndex'];

      _this.$element.find('[data-img-index=' + imgIndex + ']').css('background-image', 'url(' + selectedImage +')');
      _this.images[imgIndex] = selectedImage;
      _this.value = JSON.stringify(_this.images);
      recipe.synchronizeSave();
    });
  }

  LineItemImage.prototype.bindClick = function() {
    var _this = this;
    _this.$element.find('.holder').on('click', function() {
      var imgIndex = $(this).data('img-index'),
          selectImages = new SelectImages(_this, imgIndex);

      selectImages.initialize();
    });
  }

  LineItemImage.prototype.bindDelete = function() {
    var _this = this;
    _this.$element.find('.delete').on('click', function() {
      _this.destroy = true;
      recipe.removeLineItem(_this.lineItemName);
    });
  }

  return LineItemImage;

})();
