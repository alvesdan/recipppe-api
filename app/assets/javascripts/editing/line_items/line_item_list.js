'use strict';
var LineItemList;
LineItemList = (function() {

  var sharedBehaviors = null;
  var template = '<{{tag}} data-name="{{name}}" data-type="list" data-position="{{position}}" class="line-item">{{value}}</{{tag}}>';
  var childTemplate = '<li contenteditable="true">{{value}}</li>';

  function LineItemList(lineItemName, tag, value) {
    this.itemType = 'list';
    this.lineItemName = lineItemName;
    this.tag = tag;
    this.position = recipe.nextAvailablePosition();
    this.$element = null;
    this.destroy = false;
    this.value = value || '';
  }

  LineItemList.prototype.setValue = function(value) {
    this.value = value;
    if (value.length) {
      var jsonValue = JSON.parse(value);
      this.$element.html('');
      for (var key in jsonValue) {
        var childValue = jsonValue[key];
        this.$element.append(Mustache.render(childTemplate, {value: childValue}));
      }
    }
  }

  LineItemList.prototype.setPosition = function(position) {
    this.position = position;
  }

  LineItemList.prototype.initialize = function() {
    sharedBehaviors = new SharedBehaviors(this.$element);
    sharedBehaviors.preventReturnClick();
    sharedBehaviors.cleanPasteFromClipboard();
    this.onKeyDown();
    this.onKeyUp();
    this.addChild();
  }

  LineItemList.prototype.setFocus = function() {
    this.focusLastChild();
  }

  LineItemList.prototype.createElementTag = function() {
    this.$element = $(Mustache.render(template, {
      name: this.lineItemName,
      tag: this.tag,
      value: this.value,
      position: this.position
    }));
  }

  LineItemList.prototype.addChild = function() {
    this.$element.append(Mustache.render(childTemplate, {value: ''}));
    this.focusLastChild();
  }

  LineItemList.prototype.removeChild = function(targetChild) {
    if (this.$element.find('li').length > 1) {
      this.focusPreviousChild(targetChild);
      targetChild.remove();
    } else {
      this.destroy = true;
      recipe.removeLineItem(this.lineItemName);
    }
  }

  LineItemList.prototype.focusPreviousChild = function(currentChild) {
    var $targetChild = currentChild.prev('li');
    $targetChild.focus();
    globalMethods.placeCaretAtEnd($targetChild[0]);
  }

  LineItemList.prototype.focusLastChild = function() {
    var $targetChild = this.$element.find('li:last');
    $targetChild.focus()
    globalMethods.placeCaretAtEnd($targetChild[0]);
  }

  LineItemList.prototype.onKeyDown = function() {
    var klass = this;
    $(document).on('keydown', '[data-name=' + klass.lineItemName + '] li', function(event) {
      if (event.keyCode == 13) {
        klass.addChild();
      } else if (event.keyCode == 8) {
        if ($(this).html().trim() == '') {
          event.preventDefault();
          klass.removeChild($(this));
        }
      }
    });
  }

  LineItemList.prototype.onKeyUp = function() {
    var klass = this;
    $(document).on('keyup', '[data-name=' + klass.lineItemName + '] li', function(event) {
      klass.value = klass.extractValue();
      recipe.synchronizeSave();
    });
  }

  LineItemList.prototype.extractValue = function() {
    var elements = [];
    this.$element.find('li').each(function(){
      elements.push($(this).html().trim().cleanHTML());
    });
    return JSON.stringify(elements);
  }

  return LineItemList;

})();
