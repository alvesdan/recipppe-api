'use strict';
var GlobalMethods;
GlobalMethods = (function(){

  function GlobalMethods() {}

  GlobalMethods.prototype.isJqueryObject = function(element) {
    return element instanceof jQuery;
  }

  GlobalMethods.prototype.capitalize = function(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  GlobalMethods.prototype.constantize = function(class_name, name, tag) {
    return eval('new ' + class_name + '("' + name + '", "' + tag +'")');
  }

  GlobalMethods.prototype.placeCaretAtEnd = function(element) {
    element.focus();
    if (typeof window.getSelection != 'undefined' && typeof document.createRange != 'undefined') {
      var range = document.createRange();
      range.selectNodeContents(element);
      range.collapse(false);
      var selection = window.getSelection();
      selection.removeAllRanges();
      selection.addRange(range);
    } else if (typeof document.body.createTextRange != 'undefined') {
      var textRange = document.body.createTextRange();
      textRange.moveToElementText(element);
      textRange.collapse(false);
      textRange.select();
    }
  }

  return GlobalMethods;

})();

var globalMethods = new GlobalMethods();
