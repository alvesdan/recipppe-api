'use strict';
var Sort;
Sort = (function() {

  function Sort(element) {
    this.$element = $(element);
    this.sortableObject = null;
    this.enabled = false;
  }

  Sort.prototype.enable = function() {
    this.disableEditing();
    $('section.content').addClass('sorting');
    this.sortableObject = new Sortable(this.$element[0], {
      draggable: '.line-item'
    });
  }

  Sort.prototype.disable = function() {
    this.enableEditing();
    $('section.content').removeClass('sorting');
    this.sortableObject.destroy();
    recipe.updatePositions();
    recipe.synchronizeSave();
  }

  Sort.prototype.disableEditing = function() {
    this.$element.find('[contenteditable]').each(function(){
      $(this).attr('contenteditable', 'false');
    });
  }

  Sort.prototype.enableEditing = function() {
    this.$element.find('[contenteditable]').each(function(){
      $(this).attr('contenteditable', 'true');
    });
  }

  Sort.prototype.toggle = function() {
    if (this.enabled) { this.disable(); }
    else { this.enable(); }
    this.enabled = !this.enabled;
  }

  return Sort;

})();
