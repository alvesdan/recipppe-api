'use strict';
var Recipe;
Recipe = (function(){

  var synchronizeTimeout = null;
  var resourceURL = '/api/recipes';
  var inputs = [
    'a', 'b', 'b', 'd', 'e', 'f', 'g', 'h', 'i',
    'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
    's', 't', 'u', 'v', 'x', 'z', 'w', 'y',
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9
  ];

  function Recipe() {
    this.modelAttributes = {
      name: '',
      category_id: '',
      servings: '',
      preparation_time: '',
      published: false,
      line_items: {}
    }
  }

  Recipe.prototype.updateAtribute = function(attribute, value) {
    this.modelAttributes[attribute] = value;
    this.synchronizeSave();
  }

  Recipe.prototype.createLineItem = function(line_item_type, tag) {
    var name = this.generateUniqueName();
    var class_name = 'LineItem' + globalMethods.capitalize(line_item_type);
    return globalMethods.constantize(class_name, name, tag);
  }

  Recipe.prototype.findLineItem = function(name) {
    return this.modelAttributes['line_items'][name];
  }

  Recipe.prototype.loadLineItem = function(name, line_item_type, tag, value) {
    var class_name = 'LineItem' + globalMethods.capitalize(line_item_type);
    var loadedLineItem = globalMethods.constantize(class_name, name, tag);
    loadedLineItem.createElementTag();
    loadedLineItem.initialize();
    loadedLineItem.setValue(value);
    this.modelAttributes['line_items'][loadedLineItem.lineItemName] = loadedLineItem;
    $defaultTarget.append(loadedLineItem.$element);
    loadedLineItem.setFocus();
  }

  Recipe.prototype.addLineItem = function(line_item_type, tag) {
    var newLineItem = this.createLineItem(line_item_type, tag);
    newLineItem.createElementTag();
    newLineItem.initialize();
    this.modelAttributes['line_items'][newLineItem.lineItemName] = newLineItem;
    $defaultTarget.append(newLineItem.$element);
    newLineItem.setFocus();
    this.updatePositions();
    this.synchronizeSave();
  }

  Recipe.prototype.removeLineItem = function(name) {
    var targetLineItem = this.findLineItem(name);
    var previousLineItem = this.findOnPosition(targetLineItem.position - 1);
    targetLineItem.$element.remove();
    // Instead of deleting the element, we set destroy
    // as true in object class
    // delete(this.modelAttributes['line_items'][name]);
    if (previousLineItem) { previousLineItem.setFocus(); }
    this.updatePositions();
    this.synchronizeSave();
  }

  Recipe.prototype.nextAvailablePosition = function() {
    var size = -1;
    for (var key in this.modelAttributes['line_items']) { size++; }
    return size + 1;
  }

  Recipe.prototype.updatePositions = function() {
    var klass = this;
    var index = 0;
    $('[data-position]').each(function() {
      var name = $(this).attr('data-name');
      var lineItem = klass.findLineItem(name);
      lineItem.setPosition(index);
      $(this).attr('data-position', index);
      index++;
    });
  }

  Recipe.prototype.findOnPosition = function(position) {
    var target = null;
    $.each(this.modelAttributes['line_items'], function(key, lineItem) {
      if (lineItem.position == position) { target = lineItem; }
    });
    return target;
  }

  Recipe.prototype.generateName = function() {
    var code = $.map([1, 2, 3, 4], function() {
      return inputs[Math.floor(Math.random() * inputs.length)]
    });
    return code.join('');
  }

  Recipe.prototype.generateUniqueName = function() {
    var uniqueCode = this.generateName();
    while (this.modelAttributes['line_items'].hasOwnProperty(uniqueCode)) {
      uniqueCode = this.generateCode();
    }
    return uniqueCode;
  }

  Recipe.prototype.lineItemsArray = function() {
    var exportArray = [];
    $.each(this.modelAttributes['line_items'], function(key, lineItem) {
      var lineItemHash = {
        name: lineItem.lineItemName,
        item_type: lineItem.itemType,
        tag: lineItem.tag,
        content: lineItem.value,
        position: lineItem.position
      }
      if (lineItem.destroy) { lineItemHash['_destroy'] = '1'; }
      exportArray.push(lineItemHash);
    });
    return exportArray;
  }

  Recipe.prototype.exportHash = function() {
    var ma = this.modelAttributes;
    return {
      name: ma['name'],
      category_id: ma['category_id'],
      servings: ma['servings'],
      preparation_time: ma['preparation_time'],
      published: ma['published'],
      line_items_attributes: this.lineItemsArray()
    }
  }

  Recipe.prototype.loadRecipe = function() {
    return $.ajax({
      url: resourceURL + '/' + recipeId,
      type: 'get',
      dataType: 'json',
      async: false
    }).responseJSON;
  }

  Recipe.prototype.synchronizePull = function() {
    var recipeData = this.loadRecipe();
    this.modelAttributes['name'] = recipeData.name;
    this.modelAttributes['preparation_time'] = recipeData.preparation_time;
    this.modelAttributes['servings'] = recipeData.servings;
    this.modelAttributes['published'] = recipeData.published;
    this.modelAttributes['category_id'] = recipeData.category_id.$oid;
    for (var key in recipeData.line_items) {
      var lineItem = recipeData.line_items[key];
      this.loadLineItem(lineItem['_id'], lineItem.item_type, lineItem.tag, lineItem.content);
    }
  }

  Recipe.prototype.synchronizeSave = function() {
    var klass = this;
    clearInterval(synchronizeTimeout);
    synchronizeTimeout = setTimeout(function() {
      klass.persist();
    }, 2000);
  }

  Recipe.prototype.persist = function() {
    var klass = this;
    $.ajax({
      url: resourceURL + '/' + recipeId,
      type: 'patch',
      dataType: 'json',
      data: { recipe: klass.exportHash() },
      success: function(response) {
        //console.log(response);
      }
    });
  }

  return Recipe;

})();
