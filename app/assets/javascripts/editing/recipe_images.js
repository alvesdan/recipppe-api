'use strict';
var RecipeImages;
RecipeImages = (function(){

  var placeHolderTemplate = '<div class="col-xs-4">' +
    '<div class="thumbnail placeholder" data-id="{{id}}">' +
      '&nbsp;' +
    '</div>' +
  '</div>';

  var thumbnailTemplate = '<div class="col-xs-4">' +
    '<div class="thumbnail" data-id="{{id}}" data-url="{{url}}">' +
      '<img src="{{src}}" />' +
      '<a href="#" class="delete"><span class="glyphicon glyphicon-trash"></span></a>' +
    '</div>' +
  '</div>';

  function RecipeImages() {
    this.imagesDropZone = this.createDropZone();
    this.bindProcessing();
    this.bindSuccess();
    this.bindError();
    this.bindComplete();
  }

  RecipeImages.prototype.createDropZone = function() {
    return new Dropzone('div.recipe-images-upload', {
      url: '/api/recipes/' + recipeId +'/recipe_images.json',
      method: 'post',
      paramName: 'image',
      maxFiles: 5,
      maxFilesize: 1,
      parallelUploads: 1,
      uploadMultiple: true,
      clickable: '.recipe-images-upload .clickable',
      headers: {
        'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content')
      },
      acceptedFiles: 'image/png,image/jpeg,image/gif',
      previewsContainer: '.recipe-images-upload .upload-details',
      createImageThumbnails: true,
      maxThumbnailFilesize: 1,
      thumbnailWidth: 200,
      thumbnailHeight: 200
    });
  }

  RecipeImages.prototype.bindProcessing = function() {
    var _this = this;
    this.imagesDropZone.on('addedfile', function() {
      var renderedTemplate = Mustache.render(placeHolderTemplate, {id: '_'});
      $('.recipe-images-upload .clickable').parent().before(renderedTemplate);
    });
  }

  RecipeImages.prototype.bindSuccess = function() {
    var _this = this;
    this.imagesDropZone.on('success', function(_, response) {
      var renderedTemplate = Mustache.render(thumbnailTemplate, {
        id: response['_id']['$oid'],
        src: response['image']['medium']['url'],
        url: response['image']['large']['url']
      });
      $('.recipe-images-upload .thumbnail.placeholder:first').parent().replaceWith(renderedTemplate);
    });
  }

  RecipeImages.prototype.bindError = function() {
    this.imagesDropZone.on('error', function() {
      $('.recipe-images-upload .thumbnail.placeholder:first').parent().remove();
    });
  }

  RecipeImages.prototype.bindComplete = function() {
    this.imagesDropZone.on('complete', function() {
      //
    });
  }

  return RecipeImages;

})();
