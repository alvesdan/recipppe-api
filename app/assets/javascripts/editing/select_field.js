'use strict';
var SelectField;
SelectField = (function() {

  var selectTemplate = '<div class="dropdown dropup"><a data-toggle="dropdown" href="#">{{selected}}</a><ul class="dropdown-menu" role="menu" aria-labelledby="dLabel"></div>'
  var optionTemplate = '<li><a href="#" data-value="{{value}}">{{label}}</a></li>';

  function SelectField(element) {
    this.$element = $(element);
    this.initialize();
  }

  SelectField.prototype.initialize = function() {
    this.buildDropdown();
    var resourceUrl = this.$element.attr('data-resource-url');
    if (resourceUrl) {
      this.populateFromResource(resourceUrl);
    } else {
      this.populateFromElement();
    }
    this.bindItemSelected();
  }

  SelectField.prototype.buildDropdown = function() {
    var renderedTemplate = $(Mustache.render(selectTemplate, {selected: this.$element.attr('data-default')}));
    this.$dropdown = renderedTemplate.find('.dropdown-menu');
    this.$element.append(renderedTemplate);
  }

  SelectField.prototype.populateFromResource = function(resourceUrl) {
    var resource = this.loadResource(resourceUrl);
    for (var key in resource) {
      var item = resource[key];
      this.addOption(item._id.$oid, item.name);
    }
  }

  SelectField.prototype.populateFromElement = function() {
    var tags = this.$element.attr('data-resource-content').split(',');
    for (var key in tags) {
      var tag = tags[key];
      this.addOption(tag, tag);
    }
  }

  SelectField.prototype.addOption = function(value, label) {
    var optionTag = $(Mustache.render(optionTemplate, {
      value: value,
      label: label
    }));
    this.$dropdown.append(optionTag);
  }

  SelectField.prototype.loadResource = function(resourceUrl) {
    return $.ajax({
      url: resourceUrl,
      type: 'get',
      dataType: 'json',
      async: false
    }).responseJSON;
  }

  SelectField.prototype.bindItemSelected = function() {
    var klass = this;
    this.$dropdown.find('a').on('click', function(event) {
      var fieldName = klass.$element.attr('data-name');
      var value = $(this).attr('data-value');
      var label = $(this).html();
      recipe.updateAtribute(fieldName, value);
      klass.clearActiveItem();
      klass.setActiveItem(this);
      klass.updateCurrentLabel(label);
      event.preventDefault();
    });
  }

  SelectField.prototype.clearActiveItem = function() {
    this.$dropdown.find('.active').removeClass('active');
  }

  SelectField.prototype.setActiveItem = function(item) {
    $(item).parent().addClass('active');
  }

  SelectField.prototype.updateCurrentLabel = function(label) {
    this.$element.find('[data-toggle]').html(label);
  }

  return SelectField;

})();
