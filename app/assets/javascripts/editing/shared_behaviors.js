'use strict';
var SharedBehaviors;
SharedBehaviors = (function(){

  function SharedBehaviors(element) {
    this.$element = element;
  }

  /*
   * Prevent return [enter] click
   * Example use:
   * new SharedBehaviors(myElement).preventReturnClick();
   */
  SharedBehaviors.prototype.preventReturnClick = function() {
    this.$element.on('keydown', function(event) {
      if (event.keyCode == 13) {
        event.preventDefault();
      }
    });
  }

  SharedBehaviors.prototype.cleanPasteFromClipboard = function() {
    var klass = this;
    this.$element.on('paste', function(event) {
      event.preventDefault();
      if (event.originalEvent.clipboardData && event.originalEvent.clipboardData.getData) {
        var html = [];
        var paragraphs = event.originalEvent.clipboardData.getData('text/plain').split(/[\r\n]/g);
        for (var index in paragraphs) {
          var p = paragraphs[index];
          html.push(p);
          document.execCommand('insertHTML', false, html.join('. ').cleanHTML());
          setTimeout(function() {
            klass.unwrapTags();
          }, 10);
        }
      }
    });
  }

  SharedBehaviors.prototype.unwrapTags = function() {
    this.$element.find('*').each(function() {
      if ($(this).prop('tagName') != 'LI') {
        $(this).contents().unwrap();
      }
    });
  }

  return SharedBehaviors;

})();
