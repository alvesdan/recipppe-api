$(function(){
  $('.new-recipe').on('click', function(event) {
    event.preventDefault();
    $('#newRecipe').modal({show: true});
  });

  $('.recipe-thumbnail').each(function() {
    var imageElement = $(this).find('.image');
    var backgroundURL = imageElement.data('background-url');
    if (backgroundURL) {
      imageElement.css({
        'background-image': 'url(' + backgroundURL +')'
      });
    }
  });
});
