$(function() {
  $('.profile .user-image').each(function() {
    var backgroundURL = $(this).data('background-url');
    if (backgroundURL) {
      $(this).css({
        'background-image': 'url(' + backgroundURL +')'
      });
    }
  });

  $('.open-account-settings').on('click', function(event) {
    event.preventDefault();
    $('#accountSettings').modal({show: true});
  });
});
