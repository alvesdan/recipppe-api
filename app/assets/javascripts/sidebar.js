'use strict';
var Sidebar;
Sidebar = (function() {

  function Sidebar() {
    this.createSidebarEvents();
  }

  Sidebar.prototype.toggle = function() {
    $('body').toggleClass('sidebar-opened');
  }

  Sidebar.prototype.open = function() {
    $('body').addClass('sidebar-opened');
  }

  Sidebar.prototype.close = function() {
    $('body').removeClass('sidebar-opened');
  }

  Sidebar.prototype.createSidebarEvents = function() {
    var _this = this;
    $('.header .open-sidebar').on('click', function(event) {
      event.preventDefault();
      _this.toggle();
    });

    $('.sidebar .btn.close').on('click', function(event) {
      event.preventDefault();
      _this.close();
    });
  }

  return Sidebar;

})();
