'use strict';
var Layout;
Layout = (function() {

  function Layout() {
    this.initialize();
  }

  Layout.prototype.initialize = function() {
    this.handleWindowResize();
  }

  Layout.prototype.handleWindowResize = function() {
    $(window).off('resize');
    $(window).on('resize', function() {
      if ($(this).width() >= 1024) {
        $('body').find('.recipe .content').height($(window).height());
      }
    });
    $(window).trigger('resize');
  }

  return Layout;

})();
