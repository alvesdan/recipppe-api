config = YAML.load_file(Rails.root + 'config/facebook.yml')
config = config[Rails.env]

Rails.application.config.middleware.use OmniAuth::Builder do
  provider :developer unless Rails.env.production?
  provider :facebook, config['app_id'], config['app_secret']
end