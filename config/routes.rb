RecipppeApi::Application.routes.draw do

  get '/auth/facebook/callback', to: 'users#create'
  get '/signout', to: 'sessions#destroy', as: :signout
  root to: 'users#new'

  resources :recipes, only: [:index, :create, :edit, :destroy]

  namespace :api do
    resources :categories, only: [:index]
    resources :recipes, only: [:show, :create, :update, :destroy] do
      post :cover, on: :member
      resources :recipe_images, only: [:index, :create, :destroy]
    end
  end

end
